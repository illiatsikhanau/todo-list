export interface Todo {
    text: string
    type: TodoType
}

export enum TodoType {
    All,
    Active,
    Completed
}
