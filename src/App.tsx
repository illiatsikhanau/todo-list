import './App.scss';
import React, {KeyboardEvent, useState} from 'react';
import {Todo, TodoType} from './todo';
import {Checkbox, Flex, Heading, Input, List, ListItem, Radio, RadioGroup, Stack} from '@chakra-ui/react';

const App = () => {
    const [todo, setTodo] = useState('');
    const [todos, setTodos] = useState([] as Todo[]);
    const [todosFilter, setTodosFilter] = useState(TodoType.All);

    const onAddTodo = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            setTodos([...todos, {text: todo, type: TodoType.Active}]);
            setTodo('');
        }
    };

    const onCompleteTodo = (todoIndex: number) => {
        const newTodos = [...todos];
        newTodos[todoIndex].type = TodoType.Completed;
        setTodos(newTodos);
    };

    return (
        <Flex
            flexDirection='column'
            alignItems='center'
            gap='6'
            p='6'
            boxShadow='md'
            rounded='md'
            border='2px'
            borderColor='gray.200'
        >
            <Heading size='md'>Todo List</Heading>
            <Input
                value={todo}
                onChange={e => setTodo(e.target.value)}
                onKeyDown={onAddTodo}
                placeholder='What needs to be done?'
                size='sm'
                autoFocus
                data-testid='input'
            />
            <List w='100%'>
                {todos.map((todo, todoIndex) =>
                    <ListItem key={todoIndex} hidden={todo.type !== todosFilter && todosFilter !== TodoType.All}>
                        <Checkbox
                            onChange={() => onCompleteTodo(todoIndex)}
                            checked={todo.type === TodoType.Completed}
                            disabled={todo.type === TodoType.Completed}
                            w='100%'
                            data-testid='todo'
                        >
                            {todo.text}
                        </Checkbox>
                    </ListItem>
                )}
            </List>
            <RadioGroup defaultValue={TodoType.All.toString()} onChange={e => {
                setTodosFilter(parseInt(e))
                console.log(todos)
            }}>
                <Stack direction='row'>
                    <Radio value={TodoType.All.toString()} data-testid='all'>All</Radio>
                    <Radio value={TodoType.Active.toString()} data-testid='active'>Active</Radio>
                    <Radio value={TodoType.Completed.toString()} data-testid='completed'>Completed</Radio>
                </Stack>
            </RadioGroup>
        </Flex>
    );
}

export default App;
