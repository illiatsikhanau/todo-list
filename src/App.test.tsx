import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from './App';

test('All, Active and Completed todos with 1 todo added.', async () => {
    render(<App/>);
    const input = screen.getByTestId('input');

    await userEvent.type(input, 'todo1{enter}');
    const todo1 = screen.getByText('todo1');

    expect(todo1).toBeVisible();

    await userEvent.click(screen.getByTestId('active'));
    expect(todo1).toBeVisible();

    await userEvent.click(screen.getByTestId('completed'));
    expect(todo1).not.toBeVisible();
});

test('All, Active and Completed todos with 2 todos added.', async () => {
    render(<App/>);
    const input = screen.getByTestId('input');

    await userEvent.type(input, 'todo1{enter}');
    const todo1 = screen.getByText('todo1');
    await userEvent.type(input, 'todo2{enter}');
    const todo2 = screen.getByText('todo2');

    await userEvent.click(todo1);

    expect(todo1).toBeVisible();
    expect(todo2).toBeVisible();

    await userEvent.click(screen.getByTestId('active'));
    expect(todo1).not.toBeVisible();
    expect(todo2).toBeVisible();

    await userEvent.click(screen.getByTestId('completed'));
    expect(todo1).toBeVisible();
    expect(todo2).not.toBeVisible();
});

test('All, Active and Completed todos with 3 todos added.', async () => {
    render(<App/>);
    const input = screen.getByTestId('input');

    await userEvent.type(input, 'todo1{enter}');
    const todo1 = screen.getByText('todo1');
    await userEvent.click(todo1);
    await userEvent.type(input, 'todo2{enter}');
    const todo2 = screen.getByText('todo2');
    await userEvent.type(input, 'todo3{enter}');
    const todo3 = screen.getByText('todo3');
    await userEvent.click(todo3);
    await userEvent.click(todo3);

    expect(todo1).toBeVisible();
    expect(todo2).toBeVisible();
    expect(todo3).toBeVisible();

    await userEvent.click(screen.getByTestId('active'));
    expect(todo1).not.toBeVisible();
    expect(todo2).toBeVisible();
    expect(todo3).not.toBeVisible();

    await userEvent.click(screen.getByTestId('completed'));
    expect(todo1).toBeVisible();
    expect(todo2).not.toBeVisible();
    expect(todo3).toBeVisible();

    await userEvent.click(screen.getByTestId('all'));
    expect(todo1).toBeVisible();
    expect(todo2).toBeVisible();
    expect(todo3).toBeVisible();
});